#!/bin/bash

export ANSIBLE_NOCOWS=1

pip install -r requirements.txt

ansible-playbook -i hosts deploy_iam.yml
ansible-playbook -i hosts deploy_common_infrastructure.yml
ansible-playbook -i hosts deploy_gitlab_runner.yml
ansible-playbook -i hosts deploy_gitlab_schedules.yml
